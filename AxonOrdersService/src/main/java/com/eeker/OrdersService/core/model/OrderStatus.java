
package com.eeker.OrdersService.core.model;

public enum OrderStatus {
    CREATED, APPROVED, REJECTED
}